package br.com.iesb.checkersboard.controllers;

import br.com.iesb.checkersboard.dtos.*;
import br.com.iesb.checkersboard.services.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class GameController {

    private final GameService gameService;

    @PostMapping("/settings")
    public Mono<TableResponseDTO> config(@RequestBody SettingsDTO config) {
        return Mono.just(gameService.setConfig(config));
    }

    @PostMapping("/current-state")
    public Mono<TableResponseDTO> userMove(@RequestBody GameSessionDTO gameSessionDTO) {

        log.info("Received: {}", gameSessionDTO);

        return Mono.just(gameService.getCurrentState(gameSessionDTO));
    }

    @PostMapping("/user-move")
    public Mono<TableResponseDTO> userMove(@RequestBody UserMoveDTO userMoveDTO) {

        log.info("Received: {}", userMoveDTO);

        return Mono.just(gameService.doUserMovement(userMoveDTO));
    }

    @PostMapping("/minimax-move")
    public Mono<TableResponseDTO> minimaxMove(@RequestBody GameSessionDTO gameSessionDTO) throws Exception {

        log.info("Received: {}", gameSessionDTO);

        return Mono.just(gameService.doMinimaxMove(gameSessionDTO));
    }

    @PostMapping("/possible-moves")
    public Mono<List<PossibleMovesDTO>> getPossbileMoves(@RequestBody AutomatedMoveDTO automatedMoveDTO) {

        log.info("Received: {}", automatedMoveDTO);

        return Mono.just(gameService.getPossibleMoves(automatedMoveDTO));
    }
}
