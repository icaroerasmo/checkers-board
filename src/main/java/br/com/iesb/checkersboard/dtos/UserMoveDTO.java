package br.com.iesb.checkersboard.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import br.com.iesb.enums.Direction;

import java.util.List;

@Data
@NoArgsConstructor
public class UserMoveDTO extends GameSessionDTO {
   private Integer line;
   private Integer column;
   private List<Direction> directions;
}
