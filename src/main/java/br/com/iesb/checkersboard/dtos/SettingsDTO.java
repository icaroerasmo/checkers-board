package br.com.iesb.checkersboard.dtos;

import br.com.iesb.enums.MinimaxDifficultyLevel;
import br.com.iesb.enums.PlayerTurn;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SettingsDTO extends GameSessionDTO {
    private MinimaxDifficultyLevel difficultyLevel;
    private PlayerTurn player;
}
