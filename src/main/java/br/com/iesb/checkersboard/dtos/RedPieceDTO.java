package br.com.iesb.checkersboard.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RedPieceDTO extends PieceDTO {
}
