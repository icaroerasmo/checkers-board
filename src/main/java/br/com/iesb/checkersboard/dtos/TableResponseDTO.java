package br.com.iesb.checkersboard.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TableResponseDTO {
    private MovesCoreDTO movesCore;
    private List<String> movesLog;
    private int captures;
}
