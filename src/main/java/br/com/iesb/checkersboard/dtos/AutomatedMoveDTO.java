package br.com.iesb.checkersboard.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AutomatedMoveDTO extends GameSessionDTO{
    private Integer line;
    private Integer column;
}
