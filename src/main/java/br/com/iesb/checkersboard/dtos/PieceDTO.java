package br.com.iesb.checkersboard.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class PieceDTO {
    private int line;
    private int column;
    private boolean isChecker;
}
