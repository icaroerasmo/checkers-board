package br.com.iesb.checkersboard.dtos;

import java.util.LinkedList;

public class PossibleMovesDTO {
    private int captures;
    private LinkedList<MovesLogDTO> movesLog;

    public PossibleMovesDTO() {
    }

    public LinkedList<MovesLogDTO> getMovesLog() {
        return this.movesLog;
    }

    public void setMovesLog(LinkedList<MovesLogDTO> movesLog) {
        this.movesLog = movesLog;
    }

    public int getCaptures() {
        return this.captures;
    }

    public void setCaptures(int captures) {
        this.captures = captures;
    }
}
