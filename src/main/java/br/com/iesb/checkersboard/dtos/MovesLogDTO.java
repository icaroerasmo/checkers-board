package br.com.iesb.checkersboard.dtos;

import br.com.iesb.enums.Direction;
import br.com.iesb.enums.MoveType;
import br.com.iesb.enums.PlayerTurn;

public class MovesLogDTO {
    private int[] from;
    private int[] captured;
    private int[] to;
    private PlayerTurn playerTurn;
    private MoveType moveType;
    private Direction direction;
    private String description;

    public MovesLogDTO() {}

    public int[] getFrom() {
        return this.from;
    }

    public void setFrom(int[] from) {
        this.from = from;
    }

    public int[] getCaptured() {
        return this.captured;
    }

    public void setCaptured(int[] captured) {
        this.captured = captured;
    }

    public int[] getTo() {
        return this.to;
    }

    public void setTo(int[] to) {
        this.to = to;
    }

    public PlayerTurn getPlayerTurn() {
        return this.playerTurn;
    }

    public void setPlayerTurn(PlayerTurn playerTurn) {
        this.playerTurn = playerTurn;
    }

    public MoveType getMoveType() {
        return this.moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
