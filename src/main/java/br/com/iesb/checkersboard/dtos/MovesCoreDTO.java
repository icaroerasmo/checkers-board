package br.com.iesb.checkersboard.dtos;

import br.com.iesb.enums.PlayerTurn;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class MovesCoreDTO {
    private PlayerTurn playerTurn;
    private List<RedPieceDTO> redPieces;
    private List<BluePieceDTO> bluePieces;
}
