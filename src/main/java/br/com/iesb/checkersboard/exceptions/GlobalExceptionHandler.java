package br.com.iesb.checkersboard.exceptions;

import br.com.iesb.checkersboard.dtos.ErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NoMoreMovesException.class)
    protected ResponseEntity<ErrorDTO> handleNoMoreMovesException(NoMoreMovesException e) {
        return ResponseEntity.badRequest().body(new ErrorDTO(e.getMessage()));
    }
}
