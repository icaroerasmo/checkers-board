package br.com.iesb.checkersboard.exceptions;

import br.com.iesb.enums.PlayerTurn;

public class NoMoreMovesException extends Exception {

    public NoMoreMovesException(PlayerTurn playerTurn) {
        this(playerTurn, null);
    }
    public NoMoreMovesException(PlayerTurn playerTurn, Throwable throwable) {
        super("No more available moves for " + playerTurn.toString(), throwable);
    }
}
