package br.com.iesb.checkersboard.mappers;

import br.com.iesb.checkersboard.dtos.BluePieceDTO;
import br.com.iesb.checkersboard.dtos.PieceDTO;
import br.com.iesb.checkersboard.dtos.RedPieceDTO;
import br.com.iesb.table.pieces.BluePiece;
import br.com.iesb.table.pieces.Piece;
import br.com.iesb.table.pieces.RedPiece;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PieceMapper {

    @Mapping(target="line", source="line")
    @Mapping(target="column", source="column")
    @Mapping(target="isChecker", source="checker")
    RedPiece redPiece(RedPieceDTO redPieceDTO);

    @Mapping(target="line", source="line")
    @Mapping(target="column", source="column")
    @Mapping(target="checker", source="isChecker")
    RedPieceDTO redPiece(RedPiece redPiece);

    @Mapping(target="line", source="line")
    @Mapping(target="column", source="column")
    @Mapping(target="isChecker", source="checker")
    BluePiece bluePiece(BluePieceDTO bluePieceDTO);

    @Mapping(target="line", source="line")
    @Mapping(target="column", source="column")
    @Mapping(target="checker", source="isChecker")
    BluePieceDTO bluePiece(BluePiece bluePiece);
}
