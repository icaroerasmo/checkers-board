package br.com.iesb.checkersboard.mappers;

import br.com.iesb.checkersboard.dtos.TableResponseDTO;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.MovesLog;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {MovesCoreMapper.class})
public interface TableMapper {

    @Mapping(target = "movesLog", source = "movesLog", qualifiedByName = "movesLogMapper")
    TableResponseDTO response(TableState matrix);

    @Named("movesLogMapper")
    default List<String> movesLogMapper(List<MovesLog> movesLog) {
        if(movesLog == null) {
            return null;
        }
        return movesLog.stream().map(MovesLog::toString).collect(Collectors.toList());
    }
}
