package br.com.iesb.checkersboard.mappers;

import br.com.iesb.checkersboard.dtos.MovesLogDTO;
import br.com.iesb.table.moves.model.MovesLog;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface MovesLogMapper {
    @Mapping(expression = "java(movesLog.toString())", target="description")
    MovesLogDTO movesLog(MovesLog movesLog);

    List<MovesLogDTO> movesLog(List<MovesLog> movesLog);
}
