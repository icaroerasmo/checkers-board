package br.com.iesb.checkersboard.mappers;

import br.com.iesb.checkersboard.dtos.MovesCoreDTO;
import br.com.iesb.table.moves.MovesCore;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {PieceMapper.class})
public interface MovesCoreMapper {
    MovesCore movesCore(MovesCoreDTO movesCoreDTO);
    MovesCoreDTO movesCore(MovesCore movesCore);
}
