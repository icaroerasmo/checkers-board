package br.com.iesb.checkersboard.mappers;

import br.com.iesb.checkersboard.dtos.PossibleMovesDTO;
import br.com.iesb.table.moves.model.PossibleMoves;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {MovesLogMapper.class})
public interface PossibleMovesMapper {
    PossibleMovesDTO possibleMoves(PossibleMoves possibleMoves);
    List<PossibleMovesDTO> possibleMoves(List<PossibleMoves> possibleMoves);
}
