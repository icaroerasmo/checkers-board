package br.com.iesb.checkersboard.services;

import br.com.iesb.checkersboard.dtos.*;
import br.com.iesb.checkersboard.exceptions.NoMoreMovesException;
import br.com.iesb.checkersboard.mappers.PossibleMovesMapper;
import br.com.iesb.checkersboard.mappers.TableMapper;
import br.com.iesb.enums.Direction;
import br.com.iesb.table.TableState;
import br.com.iesb.table.moves.model.PossibleMoves;
import br.com.iesb.table.moves.model.UserMoveModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
@RequiredArgsConstructor
public class GameService {

    private final TableMapper tableMapper;
    private final PossibleMovesMapper possibleMovesMapper;

    private static Map<String, TableState> sessions = new HashMap<>();

    public TableResponseDTO setConfig(SettingsDTO config) {
        deleteSession(config);
        TableState tableState = getCurrentState(config.getSessionId());
        tableState.setDifficultyLevel(config.getDifficultyLevel());
        tableState.setPlayerTurn(config.getPlayer());
        saveSession(config.getSessionId(), tableState);
        return tableMapper.response(tableState);
    }

    public TableResponseDTO getCurrentState(GameSessionDTO gameSessionDTO) {
        return tableMapper.response(getCurrentState(gameSessionDTO.getSessionId()));
    }

    public TableResponseDTO doUserMovement(UserMoveDTO userMoveDTO) {

        final String sessionId = userMoveDTO.getSessionId();

        TableState currentUserState = getCurrentState(userMoveDTO.getSessionId());

        log.info("\n" + currentUserState);

        final UserMoveModel userMoveModel = new UserMoveModel(
                new int[]{userMoveDTO.getLine(), userMoveDTO.getColumn()},
                userMoveDTO.getDirections().toArray(new Direction[0]));

        TableState userTurnTable = currentUserState.doUserMove(userMoveModel);

        userTurnTable.changePlayersTurn();

        saveSession(sessionId, userTurnTable);

        return tableMapper.response(userTurnTable);
    }

    public TableResponseDTO doMinimaxMove(GameSessionDTO gameSessionDTO) throws Exception{

        final String sessionId = gameSessionDTO.getSessionId();

        TableState currentUserState = getCurrentState(sessionId);

        log.info("\n" + currentUserState);

        TableState minimaxTurnTable;

        try {
            minimaxTurnTable = currentUserState.doMinimaxMove();
        } catch(NullPointerException e) {
            throw new NoMoreMovesException(currentUserState.getPlayerTurn(), e);
        }

        minimaxTurnTable.changePlayersTurn();

        saveSession(sessionId, minimaxTurnTable);

        return tableMapper.response(minimaxTurnTable);
    }

    public List<PossibleMovesDTO> getPossibleMoves(AutomatedMoveDTO automatedMoveDTO) {
        TableState currentUserState = getCurrentState(automatedMoveDTO.getSessionId());

        log.info("\n" + currentUserState);

        List<PossibleMoves> possibleMoves = currentUserState.getPossibleMovesForUser(automatedMoveDTO.getLine(), automatedMoveDTO.getColumn());

        return possibleMovesMapper.possibleMoves(possibleMoves);
    }

    private void saveSession(String sessionId, TableState minimaxTurnTable) {
        sessions.put(sessionId, minimaxTurnTable);
    }

    private static void deleteSession(SettingsDTO config) {
        sessions.remove(config.getSessionId());
    }

    private TableState getCurrentState(String sessionId) {

        TableState currentState = sessions.get(sessionId);

        if(currentState == null) {
            currentState = new TableState();
            saveSession(sessionId, currentState);
        }

        return currentState;
    }
}
