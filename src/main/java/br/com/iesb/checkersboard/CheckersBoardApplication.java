package br.com.iesb.checkersboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckersBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckersBoardApplication.class, args);
	}

}
